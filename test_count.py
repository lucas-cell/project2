import unittest
from argparse import Namespace
from count import count_characters

class TestCountCharacters(unittest.TestCase):

    def test_basic(self):
        args = type('', (), {'files': ['test_file.txt'], 'c': False, 'l': None, 'z': False})()
        expected_output = {'a': 2, 'b': 2, 'c': 2, 'd': 2}
        self.assertEqual(count_characters(args), expected_output)

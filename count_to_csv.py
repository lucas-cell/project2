#!/usr/bin/env python3

import argparse
import count
import csv

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Count character frequencies in one or more text files and write to CSV.')
    parser.add_argument('-c', action='store_true', help='distinguish between upper and lower case')
    parser.add_argument('-l', type=str, metavar='letters', help='only print given letters')
    parser.add_argument('-z', action='store_true', help='print zeros for missing letters')
    parser.add_argument('--no-flag', action='store_true', help='count frequencies without any flags')
    parser.add_argument('files', type=str, nargs='+', help='input files')
    parser.add_argument('output_file', type=str, help='output CSV file')

    args = parser.parse_args()

    if args.no_flag:
        args.c = False
        args.l = None
        args.z = False

    char_freq_dict = count.count_characters(args)

    with open(args.output_file, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(char_freq_dict.items())






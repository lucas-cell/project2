#!/usr/bin/env python3

# Student Name: Lucas Nieddu
# Student ID: 873664906
# Program Description: A program that counts the frequencies of characters in one or more text files.

import argparse
import csv
import sys


def add_frequencies(d, file, remove_case):
    """
    A function called add_frequencies that takes three parameters: a dictionary to be updated 
    with the frequency count of characters, a file path to read from, and a boolean flag indicating 
    if case sensitivity should be removed. The function reads the file and removes spaces, then, if 
    remove_case is True, it converts the data to lowercase. After that, it iterates through each character 
    in the data and adds the char as a new key with a frequency count of 1 if it doesn't already exist in 
    the dictionary, otherwise it increments the existing key's frequency count. The function then returns 
    the updated dictionary.
    """
    # Open the file and read the file into data and remove spaces from data
    with open(file, 'r') as f:
        data = f.read().replace(" ", "")

    # If remove_case is True, then transform data into lower case
    if remove_case:
        data = data.lower()

    # Count the occurrence of each character in the string data and store it in dictionary d.
    for char in data:
        if char in d:
            d[char] += 1
        else:
            d[char] = 1


def count_characters(args):
    """
    Handles parsing the arguments, and calling add_frequencies(...). It should perform the following steps:

        1) Create an empty dictionary

        2) Add the frequencies for each file in the argument list to that dictionary

        3) If the '-l' flag is present, create a set of the given letters and filter the dictionary to 
           only include those letters.

        4) If the '-z' flag is present, add missing letters to the dictionary with a value of 0.

        5) Sort the dictionary by keys in alphabetical order

        6) Return the updated dictionary
    """
    # 1) Create an empty dictionary
    char_freq_dict = {}

    # 2) Add the frequencies for each file in the argument list to that dictionary
    for file in args.files:
        add_frequencies(char_freq_dict, file, remove_case=not args.c)

    # 3) If the '-l' flag is present, create a set of the given letters and filter the dictionary to only include those letters.
    if args.l:
        letter_set = set(args.l.lower())
        char_freq_dict = {key: value for key, value in char_freq_dict.items() if key.lower() in letter_set}

    # 4) If the '-z' flag is present, add missing letters to the dictionary with a value of 0.
    if args.z:
        all_letters = set('abcdefghijklmnopqrstuvwxyz')
        char_freq_dict.update({key: 0 for key in all_letters if key not in char_freq_dict})

    # 5) Sort the dictionary by keys in alphabetical order
    char_freq_dict = dict(sorted(char_freq_dict.items()))

    # 6) Return the updated dictionary
    return char_freq_dict


def print_csv(data):
    """
    Prints out the elements of the given dictionary in CSV format.
    """
    writer = csv.writer(sys.stdout)
    writer.writerows((f"{key}: {value}",) for key, value in data.items())


def main():
    parser = argparse.ArgumentParser(description='Count character frequencies in a text file.')
    parser.add_argument('-c', action='store_true', help='distinguish between upper and lower case')
    parser.add_argument('-l', type=str, metavar='letters', help='only print given letters')
    parser.add_argument('-z', action='store_true', help='print zeros for missing letters')
    parser.add_argument('files', type=str, nargs='+', help='input files')

    args = parser.parse_args()

    char_freq_dict = count_characters(args)

    print_csv(char_freq_dict)







